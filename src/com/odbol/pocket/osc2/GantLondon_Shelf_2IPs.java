package com.odbol.pocket.osc2;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.InetSocketAddress;
import java.net.URL;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.hardware.SensorManager;
import android.media.AudioManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.os.StrictMode;
import android.os.Vibrator;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.widget.Button;

import com.google.android.glass.eye.EyeGesture;
import com.google.android.glass.eye.EyeGestureManager;
import com.google.android.glass.eye.EyeGestureManager.Listener;
import com.google.android.glass.media.Sounds;
import com.google.android.glass.touchpad.Gesture;
import com.google.android.glass.touchpad.GestureDetector;
import com.relivethefuture.osc.data.OscMessage;
import com.relivethefuture.osc.transport.OscClient;
import com.relivethefuture.osc.transport.OscServer;

public class GantLondon_Shelf_2IPs extends Activity {

	private SensorManager mSensorManager;
	static SensorFusionActivity0_2IPs sf;
	
	private static OscClient mSender;
	private static OscClient mSender_tv;
	private OscServer server;
	private String phoneid = "phone1";
	private Vibrator vibe;
	private static int milsec = 30;
	static Button white;
	private static Button blue;
	public static boolean visible = false;
	
	private static Context con;
	static Activity acc;
	
	/**
	 * copied from TNGDemo
	 */
	private static JSONObject objectlist;//(contentid,interactiontype)
	private JSONArray namelist;
	private static JSONObject contentBOX;//记录 content的各种操蛋信息
	static OscMessage msg = null;
	
	private GestureDetector mGestureDetector;
	
	/**
	 * for Google Glass EyeGestureEetection
	 */
    private EyeGestureManager mEyeGestureManager;
    private EyeGestureListener mEyeGestureListener;
    private EyeGesture target1 = EyeGesture.WINK;
    private EyeGesture target2 = EyeGesture.DOUBLE_BLINK;
    private EyeGesture target3 = EyeGesture.LOOK_AT_SCREEN;
    private EyeGesture target4 = EyeGesture.DON;
    private EyeGesture target5 = EyeGesture.DOFF;
    private AudioManager mAudioManager;
	
	public static final int MSG_MSL_OSC_ResponseFromOSCServer = 1;
	@SuppressLint("HandlerLeak")
	public Handler msl_handler = new Handler() {
		public void handleMessage(Message msg) {
			switch (msg.what) {
			case MSG_MSL_OSC_ResponseFromOSCServer://当点击select时,在looplistener中收到osc-server中发送的id的处理...
				Intent intent = new Intent(con, GantWebView.class);
				intent.putExtra("weburl", "" + (String)msg.obj);
				acc.startActivityForResult(intent, 1);
				break;
			default: break;
			}
			super.handleMessage(msg);
		}
	};
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.movie_demo);
		
		StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
		StrictMode.setThreadPolicy(policy);
		
		this.acc = this;
		this.con = this;
		
		/**
		 * for Google Glass EyeGestureEetection
		 */
        mEyeGestureManager = EyeGestureManager.from(this);
        mEyeGestureListener = new EyeGestureListener();
        mAudioManager = (AudioManager) getSystemService(Context.AUDIO_SERVICE);
		
		mSender = new OscClient(true);
		mSender_tv = new OscClient(true);
		mSensorManager = (SensorManager) getSystemService(Context.SENSOR_SERVICE);
		
		vibe = (Vibrator) this.getSystemService(Context.VIBRATOR_SERVICE);

		white = (Button) findViewById(R.id.white);
		blue = (Button) findViewById(R.id.blue);
		
		white.setOnTouchListener(new ButtonTouch());
		blue.setOnTouchListener(new ButtonTouch());
		white.setVisibility(View.INVISIBLE);
		blue.setVisibility(View.INVISIBLE);
		
		/**
		 * network-together
		 */
		/*oscSending("/phone1/bluebutton","/phone1");
		startOSCListening();
		startListeners();*/
		
		mGestureDetector = createGestureDetector(this);
		
		new FetchPlayListFromContentmanager("").execute(MSLConfig.TempURL_Shelf_Helsinki); //TODO 2014-11-25 MSLConfig.TempURL_BigShelf_TNG
	}
	//****************************************************************************
	public class FetchPlayListFromContentmanager extends AsyncTask<String, String, String> {
	    JSONObject poster;
		
	    public FetchPlayListFromContentmanager(String list) {
	    }
	    
	    @Override
	    protected void onPreExecute() {
	        super.onPreExecute();
	    }
	    
	    @Override
		protected String doInBackground(String... urls) {
	    	String str_server = "";
	    	try {
	    		str_server = sendConnectGet(urls[0]);
				Log.v("lalala", "GLF_playlist*/"+ str_server);

			} catch (Exception e1) {
				Log.v("lalala", "exception happens 3333333333");
				e1.printStackTrace();
			}
	    	return str_server;
		}
	    
	    protected void onPostExecute(String str_server) {
	    	Log.v("gg","onPostExecute/");
	    	
	    	initializeConfigValue(str_server);//不能将此method放到doInBackground中进行!!...
	    	
	    }
	    
	    private String sendConnectGet(String url0) throws Exception {
			try{
				URL obj = new URL(url0);
				HttpURLConnection con = (HttpURLConnection) obj.openConnection();
				con.setRequestMethod("GET");
				int responseCode = con.getResponseCode();
				System.out.println("\nSending 'GET' request to URL : " + url0);
				System.out.println("Response Code : " + responseCode);

				BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
				String inputLine;
				StringBuffer response = new StringBuffer();
				while ((inputLine = in.readLine()) != null) {
					response.append(inputLine);
				}
				in.close();
				return response.toString();
			}catch(Exception ex){
				Log.v("lalala", "exception happens 44444444");
				return "";
			}
	    }
		}
	//****************************************************************************
	private void initializeConfigValue(String str_server){
		try {
			JSONObject jobj = new JSONObject(str_server);
			
			createContentList(jobj.getJSONArray("content").getJSONArray(0),  jobj.getJSONArray("poster").getJSONObject(0));
			
			MSLConfig.server_ip = jobj.getJSONArray("poster").getJSONObject(0).getString("serverip").toString();
			MSLConfig.server_port = jobj.getJSONArray("poster").getJSONObject(0).getInt("serverport");
			MSLConfig.phoneport = jobj.getJSONArray("poster").getJSONObject(0).getInt("phoneport");
			MSLConfig.server_ip_tv = jobj.getJSONArray("poster").getJSONObject(0).getString("beacon").toString();
			
			new connectToServerAsyncTask().execute("");
			
			/**
			 * network-together
			 */
			/*oscSending("/phone1/bluebutton","/phone1");
			startOSCListening();
			startListeners();*/
			
		} catch (Exception e) {
			Log.v("lalala", "exception happens 22222222222");
			e.printStackTrace();
		}
	}
	
	private void createContentList(JSONArray array, JSONObject poster) throws JSONException {
		objectlist = new JSONObject();
		namelist = new JSONArray();
		contentBOX = new JSONObject();//TNG
		objectlist.put("postertype", poster.getString("postertype"));
		objectlist.put("id", poster.getString("id"));
		for (int i = 0; i < array.length(); i++) {
			objectlist.put(array.getJSONObject(i).getString("contentid"), array.getJSONObject(i).getString("interactiontype"));
			contentBOX.put(array.getJSONObject(i).getString("contentid"), array.getJSONObject(i));//TNG
			try{
				Long.parseLong(((JSONObject)array.get(i)).getString("name"));
				namelist.put(i,(JSONObject)array.get(i) );
			}catch(NumberFormatException ex){
				ex.printStackTrace();
				continue;
			}
		}
		Log.v("test", "there are total of  objects"+ namelist.length());
	}
	//****************************************************************************
public class connectToServerAsyncTask extends AsyncTask<String,String,String>{
		
		@Override
		protected void onPreExecute() {
			SensorFusionActivity0_2IPs.recalibrate();
			super.onPreExecute();
		}

		@Override
		protected String doInBackground(String... params) {
			
			/**
			 * network-together
			 */
			oscSending("/phone1/bluebutton","/phone1");
			startOSCListening();
			startListeners();
			
			return null;
		}
		
		@Override
		protected void onPostExecute(String result) {
			super.onPostExecute(result);
		}
		
	}
	//**************************************************************************************
	//**************************************************************************************
	private GestureDetector createGestureDetector(Context context) {
	    GestureDetector gestureDetector = new GestureDetector(context);
	        //Create a base listener for generic gestures
	        gestureDetector.setBaseListener( new GestureDetector.BaseListener() {
	            @Override
	            public boolean onGesture(Gesture gesture) {
	                if (gesture == Gesture.TAP) {//select
	                	String text = "/" + phoneid + "/bigbutton";
	                	oscSending(text,0);
	                	Log.v("", "gglass/tag");
	                    return true;
	                } else if (gesture == Gesture.SWIPE_RIGHT) {//re-calibrate
	                	Log.v("", "gglass/swipe right");
	                	oscSending("/phone1/bluebutton","/phone1");//TODO
	                	SensorFusionActivity0_2IPs.recalibrate();
	                    return true;
	                } else if (gesture == Gesture.SWIPE_LEFT) {
	                	Log.v("", "gglass/swipe left");
	                	acc.finish();
	                    return true;
	                } else if (gesture == Gesture.TWO_TAP) {//select
	                	String text = "/" + phoneid + "/bigbutton";
	                	oscSending(text,0);
	                	vibe.vibrate(30);
	                	Log.v("", "gglass/two finger tag");
	                    return true;
	                }
	                return false;
	            }
	        });
	        gestureDetector.setFingerListener(new GestureDetector.FingerListener() {
	            @Override
	            public void onFingerCountChanged(int previousCount, int currentCount) {
	              // do something on finger count changes
	            	Log.v("", "gglass/onFingerCountChanged");
	            }
	        });
	        gestureDetector.setScrollListener(new GestureDetector.ScrollListener() {

				@Override
				public boolean onScroll(float displacement, float delta, float velocity) {
					// TODO Auto-generated method stub
					Log.v("", "gglass/setScrollListener/"+displacement+"/"+delta+"/"+velocity);
					return false;
				}
	        });
	        return gestureDetector;
	    }

	    /*
	     * Send generic motion events to the gesture detector
	     */
	    @Override
	    public boolean onGenericMotionEvent(MotionEvent event) {
	        if (mGestureDetector != null) {
	            return mGestureDetector.onMotionEvent(event);
	        }
	        return false;
	    }
	//**************************************************************************************
	//**************************************************************************************
	    
	    public void oscSending(String address, int org) {
	    	mSender.connect(new InetSocketAddress(MSLConfig.server_ip, MSLConfig.server_port));
	    	mSender_tv.connect(new InetSocketAddress(MSLConfig.server_ip_tv, MSLConfig.server_port));
			if(mSender==null || mSender_tv==null)
				return;
			OscMessage msg = new OscMessage(address);
			msg.addArgument(org);
			try {
				mSender.sendPacket(msg);
				mSender_tv.sendPacket(msg);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	    
	    public void oscSending(String address, String org) {
	    	mSender.connect(new InetSocketAddress(MSLConfig.server_ip, MSLConfig.server_port));
	    	mSender_tv.connect(new InetSocketAddress(MSLConfig.server_ip_tv, MSLConfig.server_port));
			if(mSender==null || mSender_tv==null)
				return;
			OscMessage msg = new OscMessage(address);
			msg.addArgument(org);
			try {
				mSender.sendPacket(msg);
				mSender_tv.sendPacket(msg);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	
	    //useless
	public class ButtonTouch implements OnTouchListener {
		@Override
		public boolean onTouch(View v, MotionEvent event) {
			if (event.getAction() == MotionEvent.ACTION_DOWN) {
				String text = "none";
				switch (v.getId()) {
				case R.id.white:
					text = "/" + phoneid + "/bigbutton";
					break;
				case R.id.blue:
					//TODO go to msl-webserver to ask
					text = "/" + phoneid + "/bluebutton";
					SensorFusionActivity0_2IPs.recalibrate();
					break;
				default:break;
				}
				
				if (!text.equals("none")) {
					vibe.vibrate(milsec);
					mSender.connect(new InetSocketAddress(MSLConfig.server_ip, MSLConfig.server_port));
					try {
						OscMessage msg = new OscMessage(text);
						msg.addArgument("/"+phoneid);
						mSender.sendPacket(msg);
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
				} else if (text.equals("none")) {
					Log.v("test", "test");
				}
			} //if
			
			return false;
		}
	}
	
	@Override
	protected void onStart() {
		super.onStart();
		/**
		 * for Google Glass EyeGestureEetection
		 */
		mEyeGestureManager.register(target1, mEyeGestureListener);
        mEyeGestureManager.register(target2, mEyeGestureListener);
	}
	
	@Override
    protected void onStop() {
        super.onStop();
        /**
    	 * for Google Glass EyeGestureEetection
    	 */
        mEyeGestureManager.unregister(target1, mEyeGestureListener);
        mEyeGestureManager.unregister(target2, mEyeGestureListener);
    }

	@Override
	protected void onResume() {
		//startOSCListening();
		oscSending("/phone1/bluebutton","/phone1");
		super.onResume();
		
	}
	
	@Override
	protected void onPause() {
		startOSCListening();
		super.onPause();
	}
	
	@Override
	protected void onDestroy() {
		stopListeners();
		stopOSCListening();
		if(mSender != null)
			mSender = null;
		if(mSender_tv != null)
			mSender_tv = null;
		super.onDestroy();
	}

//	
	private void startOSCListening() {
		stopOSCListening();
		try {
			server = new OscServer(MSLConfig.phoneport);
			server.setUDP(true);
			server.start();
		} catch (Exception e) {
			Log.v("lalala", "exception happens startOSCListening");
			return;
		}
		server.addOscListener(new LooperListener(this, GantLondon_Shelf_2IPs.objectlist, this.msl_handler));
	}

	private void stopOSCListening() {
		if (server != null) {
			server.stop();
			server = null;
		}
	}
	
	private void startListeners() {
		mSensorManager = (SensorManager) getSystemService(Context.SENSOR_SERVICE);
		if(sf!=null){
			sf.Stop();
		}
		sf = new SensorFusionActivity0_2IPs(mSensorManager, mSender, mSender_tv);
	}
	
	private void stopListeners(){
		if(sf!=null){
			sf.Stop();
			sf = null;
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.layout.tab, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case R.id.prefs:
			Intent intent = new Intent(this, OSCTesterClientPreferences.class);
			startActivity(intent);
			return true;
		}
		return false;
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		if (requestCode == 1){
		}
	}
	
	/**
	 * for Google Glass EyeGestureEetection
	 */
	private class EyeGestureListener implements Listener {
        @Override
        public void onEnableStateChange(EyeGesture eyeGesture, boolean paramBoolean) {
        }
        @Override
        public void onDetected(final EyeGesture eyeGesture) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    mAudioManager.playSoundEffect(Sounds.SUCCESS);
                    //Toast.makeText(con, ""+eyeGesture.name(), Toast.LENGTH_SHORT).show();
                    String text = "/" + phoneid + "/bigbutton";
                	oscSending(text,0);
                }
            });
        }
    }

}


