package com.odbol.pocket.osc2;


import com.google.android.glass.eye.EyeGesture;
import com.google.android.glass.eye.EyeGestureManager;
import com.google.android.glass.eye.EyeGestureManager.Listener;
import com.google.android.glass.media.Sounds;
import com.google.android.glass.touchpad.Gesture;
import com.google.android.glass.touchpad.GestureDetector;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.media.AudioManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.os.StrictMode;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup.LayoutParams;
import android.webkit.WebBackForwardList;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebSettings.LayoutAlgorithm;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.LinearLayout;
import android.widget.Toast;

@SuppressLint({ "NewApi", "SetJavaScriptEnabled" })
public class GantWebView extends Activity implements View.OnTouchListener, View.OnClickListener{

	private final String TAG = "GantWebView";
	
	private Context context;
	private Activity acc;
	float x = 0f;
	float y = 0f;
	
	private GestureDetector mGestureDetector;
	
	/**
	 * for Google Glass EyeGestureEetection
	 */
    private EyeGestureManager mEyeGestureManager;
    private EyeGestureListener mEyeGestureListener;
    private EyeGesture target1 = EyeGesture.WINK;
    private EyeGesture target2 = EyeGesture.DOUBLE_BLINK;
    private AudioManager mAudioManager;

	private LinearLayout midsection;// dynamically change the mid section
	static CustomWebView wv;
	
	private String weburl = "https://www.google.se/";
	
	private Handler webview_handler = new Handler(new Handler.Callback() {
		@Override
		public boolean handleMessage(Message msg) {
			switch (msg.what) {
			case 1:
				midsection.removeAllViews();
				
				WebSettings webs = wv.getSettings();
				webs.setJavaScriptEnabled(true);
				
				//使webview全屏,不需要再scrollView...
				webs.setUseWideViewPort(true);
				webs.setLoadWithOverviewMode(true);
				webs.setLayoutAlgorithm(LayoutAlgorithm.SINGLE_COLUMN);
				
				//webs.setRenderPriority(RenderPriority.HIGH);
				
				wv.setWebChromeClient(new WebChromeClient() {
					public void onProgressChanged(WebView view, int progress) {
							midsection.removeAllViews();
							midsection.addView(wv, new LinearLayout.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT));
					} }  );
				
				wv.setWebViewClient(new WebViewClient() {
					@Override
					public boolean shouldOverrideUrlLoading(WebView view,String url) {
						view.loadUrl(url);
						return true;
					}
					public void onReceivedError(WebView view, int errorCode,
							String description, String failingUrl) {
					}
				});
				wv.loadUrl((String) msg.obj);
				
				WebBackForwardList mWebBackForwardList = wv.copyBackForwardList();
				Log.v(TAG, "web history: " + mWebBackForwardList.getCurrentIndex());
				
				break;
			default:break;
			}
			return false;
			}
		});

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.gantwebview);
		acc = this;
		context = this;
		
		midsection = (LinearLayout) findViewById(R.id.gantwebview_midsection);
		
		/**
		 * for Google Glass EyeGestureEetection
		 */
        mEyeGestureManager = EyeGestureManager.from(this);
        mEyeGestureListener = new EyeGestureListener();
        mAudioManager = (AudioManager) getSystemService(Context.AUDIO_SERVICE);

		//initialize the GantWebView
		wv = new CustomWebView(this);
		wv.setOnTouchListener(this);
		weburl = this.getIntent().getExtras().getString("weburl");
		Message msg = new Message();
		msg.what = 1;
		msg.obj = weburl;
		webview_handler.sendMessage(msg);

		StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
		StrictMode.setThreadPolicy(policy);
		
		mGestureDetector = createGestureDetector(this);
		
	} // onCreate
	
	@Override
    protected void onStart() {
        super.onStart();
        mEyeGestureManager.register(target1, mEyeGestureListener);
        mEyeGestureManager.register(target2, mEyeGestureListener);
    }
	
	@Override
    protected void onStop() {
        super.onStop();
        mEyeGestureManager.unregister(target1, mEyeGestureListener);
        mEyeGestureManager.unregister(target2, mEyeGestureListener);
    }
	//***
	
	private GestureDetector createGestureDetector(Context context) {
	    GestureDetector gestureDetector = new GestureDetector(context);
	        //Create a base listener for generic gestures
	        gestureDetector.setBaseListener( new GestureDetector.BaseListener() {
	            @Override
	            public boolean onGesture(Gesture gesture) {
	                if (gesture == Gesture.TAP) {
	                	Log.v("", "gglass/tag");
	                    return true;
	                } else if (gesture == Gesture.SWIPE_RIGHT) {
	                	Log.v("", "gglass/swipe right");
	                    return true;
	                } else if (gesture == Gesture.SWIPE_LEFT) {//exit
	                	acc.finish();
	                	Log.v("", "gglass/swipe left");
	                    return true;
	                } else if (gesture == Gesture.TWO_TAP) {
	                	Log.v("", "gglass/two finger tag");
	                    return true;
	                }
	                return false;
	            }
	        });
	        gestureDetector.setFingerListener(new GestureDetector.FingerListener() {
	            @Override
	            public void onFingerCountChanged(int previousCount, int currentCount) {
	              // do something on finger count changes
	            }
	        });
	        gestureDetector.setScrollListener(new GestureDetector.ScrollListener() {

				@Override
				public boolean onScroll(float displacement, float delta, float velocity) {
					// TODO Auto-generated method stub
					return false;
				}
	        });
	        return gestureDetector;
	    }

	    /*
	     * Send generic motion events to the gesture detector
	     */
	    @Override
	    public boolean onGenericMotionEvent(MotionEvent event) {
	        if (mGestureDetector != null) {
	            return mGestureDetector.onMotionEvent(event);
	        }
	        return false;
	    }
	
	public void onButtonClick_WebView(View v) {
	}

	@Override
	protected void onResume() {
		super.onResume();
	}
	
	@Override
	protected void onPause() {
		super.onPause();
	}

	@Override
	protected void onDestroy() {
		this.setResult(0);
		super.onDestroy();
	}
	
	@Override
	public void onLowMemory() {
		System.gc();
		Runtime.getRuntime().gc();
	}

	@Override
	public boolean onTouch(View v, MotionEvent event) {
		switch (event.getAction()){
		case MotionEvent.ACTION_DOWN:
			break;
		case MotionEvent.ACTION_UP:
			if(y>1650){
				Log.v(TAG, "wvaction/!!!!!!!!!!!!!!!!!!!!!!!!!!!!!");
				//this.finish();
			}else
				Log.v(TAG, "wvaction/@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@");
			break;
		case MotionEvent.ACTION_MOVE:
			x = event.getX();
			y = event.getY();
			Log.v(TAG, "wvaction/"+event.getX()+"/"+event.getY());
			break;
		default:break;
		}
		return false;
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		
	};
	
	/**
	 * for Google Glass EyeGestureEetection
	 */
	private class EyeGestureListener implements Listener {
        @Override
        public void onEnableStateChange(EyeGesture eyeGesture, boolean paramBoolean) {
        }
        @Override
        public void onDetected(final EyeGesture eyeGesture) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    mAudioManager.playSoundEffect(Sounds.SUCCESS);
                    acc.finish();
                }
            });
        }
    }

}
