package com.odbol.pocket.osc2;

import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.widget.Toast;

import com.relivethefuture.osc.data.BasicOscListener;
import com.relivethefuture.osc.data.OscMessage;

public class LooperListener extends BasicOscListener {
	
	private Activity c;
	private Handler handler;
	private JSONObject list;

	public LooperListener(Activity appActivity, JSONObject obj, Handler han) {
		c = appActivity;
		this.list = obj;
		this.handler = han;
	}

	@Override
	public void handleMessage(OscMessage msg) {
		String address = msg.getAddress();
		int index = Integer.valueOf(msg.getArguments().get(0).toString());
		
		//TODO
		if(address.equals("/playvideo")) {
			//this.handler.obtainMessage(GantLondon.MSG_MSL_OSC_ResponseFromOSCServer, MSLConfig.urls[index-1]).sendToTarget();
			
			try {
				this.handler.obtainMessage(GantLondon_1IP.MSG_MSL_OSC_ResponseFromOSCServer, list.getString(msg.getArguments().get(0).toString())).sendToTarget();
			} catch (JSONException e) {
				Toast.makeText(c, "json error", Toast.LENGTH_SHORT).show();
				this.handler.obtainMessage(GantLondon_1IP.MSG_MSL_OSC_ResponseFromOSCServer, "https://www.google.se/").sendToTarget();
				e.printStackTrace();
			}
			
			Log.v("", "msgmsg"+msg.getArguments().get(0).toString());
		}
		
	}
	
}
