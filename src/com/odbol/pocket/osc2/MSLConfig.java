package com.odbol.pocket.osc2;

import java.util.ArrayList;

public class MSLConfig {
	
	public static final String[] urls = {
		"http://msl1.it.kth.se/gant/g1.jpg",
		"http://msl1.it.kth.se/gant/g2.jpg",
		"http://msl1.it.kth.se/gant/g3.jpg",
		"http://msl1.it.kth.se/gant/g4.jpg",
		"http://msl1.it.kth.se/gant/g5.jpg",
		"http://msl1.it.kth.se/gant/g6.jpg",
		"http://msl1.it.kth.se/gant/g7.jpg",
		"http://msl1.it.kth.se/gant/g8.jpg",
		"http://msl1.it.kth.se/gant/g9jpg",
		"http://msl1.it.kth.se/gant/g10.jpg",
		"http://msl1.it.kth.se/gant/g11.jpg.old",
		"http://msl1.it.kth.se/gant/g12.jpg",
		"http://msl1.it.kth.se/gant/g13.jpg",
		"http://msl1.it.kth.se/gant/g14.jpg",
		"http://msl1.it.kth.se/gant/g15.jpg",
		"http://msl1.it.kth.se/gant/g16.jpg.old",
		"http://msl1.it.kth.se/gant/g17.jpg",
		"http://msl1.it.kth.se/gant/g18.jpg",
		"http://msl1.it.kth.se/gant/g19.jpg",
		"http://msl1.it.kth.se/gant/g20.jpg.old",
		"http://msl1.it.kth.se/gant/g21.jpg",
		"http://msl1.it.kth.se/gant/g22.jpg",
		"http://msl1.it.kth.se/gant/g23.jpg",
		"http://msl1.it.kth.se/gant/g24.jpg",};
	
	public static final String TempURL = "http://msl1.it.kth.se/contentmanager/getcontent.json?direction=north&location=2342,2342";
	public static final String TempURL_BigShelf = "http://msl1.it.kth.se/contentmanager/getcontent.json?direction=anydirection&location=1423,3241";//"BigShelf"
	
	public static final String TempURL_shelf_arduino = "http://msl1.it.kth.se/contentmanager/getcontent.json?direction=random&location=2342,2342";
	public static final String TempURL_philips = "http://192.168.0.80/database.json";
	public static final String TempURL_philips_2 = "http://192.168.2.2:8080/database.json";
	
	public static final String TempURL_TNG = "http://192.168.2.131/database2.json";
	public static final String TempURL_BigShelf_TNG = "http://192.168.2.131/database1.json";
	public static final String TempURL_Shelf_Helsinki = "http://192.168.2.130/database1.json";
	
	public static int screenOnlyForMondayDemo = 1;

	public static double direction_value = 0;
	public static double direction_value_rec = 0;//过滤后的value;
	public static String direction = "none";
	public static String get_direction_url = "http://msl1.it.kth.se/contentmanager/getcontent.json?direction=south1&location=56.2408019,-76.680299";
	
	//public static String get_location_mark = "&location=56.2408019,-76.680299";
	
	//public static String player_url = "http://130.237.238.176:81/Command.aspx?format=json&upid=0001807aadc8&command=getPlayingFiles";
	//public static String beacon_url = "&beacon=";
	//public static String bssid = "00:00:00:00:00:00";
	
	public static String server_ip = "192.168.2.5";
	public static int server_port = 50001;
	public static int phoneport = 8000;
	public static String server_ip_tv = "192.168.2.5";
	
	public static float x;
	public static float y;
	public static String direction_previous = "none";
	
	public static double direction_value2 = 0;//dengyu direction_value, zhishi butong de lei.
	public static double direction_convertered2 = 0;
	public static double converter = 0;
	public static ArrayList <Integer> drawableList = new ArrayList <Integer> ();
    public static boolean finishInitialize_ImageViews = false;
    
    public static String Timestamp;
    public static String StartTime;
    //public static long looptime = 64015;//millisecond
    //public static long offsidetvtime = 0l;
}