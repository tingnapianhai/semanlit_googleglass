/************************************************************************************
 * Copyright (c) 2012 Paul Lawitzki
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 * 
 * 
 * The above copyright notice and this permission notice shall be included
 * in all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
 * OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
 * ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE
 * OR OTHER DEALINGS IN THE SOFTWARE.
 ************************************************************************************/

package com.odbol.pocket.osc2;

import java.math.RoundingMode;
import java.net.InetSocketAddress;
import java.text.DecimalFormat;

import com.odbol.pocket.osc2.algos.Algo;
import com.odbol.pocket.osc2.algos.BufferAlgo2;
import com.relivethefuture.osc.data.OscMessage;
import com.relivethefuture.osc.transport.OscClient;

import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Handler;
import android.os.Message;
import android.util.Log;


public class SensorFusionActivity0_1IP_backup_beforeHelsinki implements SensorEventListener {
	
	/**
	 * 2014-10-31
	 * for shelf visual-search
	 */
	private static boolean INBORDER = false;
	private int recorder = 0;//2014-10-31 记录测direction的次数
	
	private float magneX = 0.0f;
	private float magneY = 0.0f;
	private float magneZ = 0.0f;
	private float magneX_pre = 0.0f;
	private float magneY_pre = 0.0f;
	private float magneZ_pre = 0.0f;
	
	private float y_pointer_pre = 0.0f;
	private float y_pointer_cha = 0.0f;
	
	private float caliX = 0;
	private float caliY = 0;
	private float caliZ = 0;
	private float caliX_pre = 0.0f;
	private float caliY_pre = 0.0f;
	private float caliZ_pre = 0.0f;
	
	/**
	 * for sensor position, for TYPE_ROTATION_VECTOR
	 */
	private static float[] rot = new float[9];
	private static float[] rot_new = new float[9];
	private float[] orie_prev = new float[3];//prev angles
	private static float[] orie = new float[3];//cur angles
	private static float[] orient = new float[3];//new
	private static float[] angle_change = new float[3];
	public Algo orientationBufferAlgo;
	float maxangle = 10.00f;
	
	/**
	 * 校准
	 */
	private float CaliX = 0.0f;
	private float CaliY = 0.0f;
	private float counter = 0.0f;//记录发送的次数
	
	static int screenImageNr = 0;//0 is poster, 2 is mannequin
	
	//TODO 只用于临时更新主UI的background-image
	private static float x_pointer_pre = 0f;
	
	/**
	 * self-define, for gant-london-real 2014-04-17
	 */
	OscMessage msg = null;
	//OscClient sender;
	OscClient sender_tv;
	static boolean ifKeepingXYPointerStill = false;//初始化时要保证normal状态,即 x&y pointer要实时更新,不应该是still
	//boolean ifServerIP_TV = false;
	
	private static SensorManager mSensorManager = null;
    private float[] gyro = new float[3];
    private float[] gyroMatrix = new float[9];
    private float[] gyroOrientation = new float[3];
	int angle_x = 0;
	int angle_y = 0;
    private float[] magnet = new float[3];
    private float[] accel = new float[3];
    private float[] accMagOrientation = new float[3];
    private float[] fusedOrientation = new float[3];
    private float[] rotationMatrix = new float[9];
    public static final float EPSILON = 0.000000001f;
    private static final float NS2S = 1.0f / 1000000000.0f;
	private long timestamp; //TODO lang ?
	private boolean initState = true;
	private static float angle[] = { 0.0f, 0.0f, 0.0f };
	private static float angle_pointer[] = { 0.0f, 0.0f, 0.0f };
	public static final int TIME_CONSTANT = 30;
	public static final float FILTER_COEFFICIENT = 0.7f;
	//private Timer fuseTimer = new Timer();
	private static float x_pointer = 0;
	private static float y_pointer = 0;
	private static float pointerShiftX = -5f;
	private float x_pointer_still = 0;
	private float y_pointer_still = 0;
	//private static float pointerShift = 0f;
	calculateFusedOrientationTask task;
	
	private static int X_Pointer_index = 1;//used to be 2...
	private static int Y_Pointer_index = 0;
	
	// The following members are only for displaying the sensor output.
	//public Handler mHandler;
	DecimalFormat d = new DecimalFormat("#.##");
	
    public SensorFusionActivity0_1IP_backup_beforeHelsinki(SensorManager sm, OscClient oscClient_tv) {
    	recorder = 0;
    	
    	counter = 0;
    	/**
    	 * self-define, for gant-london-real 2014-04-17
    	 */
    	//sender = oscClient;
    	//sender.connect(new InetSocketAddress(MSLConfig.server_ip, MSLConfig.server_port));
    	sender_tv = oscClient_tv;
    	sender_tv.connect(new InetSocketAddress(MSLConfig.server_ip_tv, MSLConfig.server_port));
    	//ifSendOSCPositionMessage = true;//初始化时要保证normal状态,即 可以发送OSC message
    	//ifKeepingXYPointerStill = false;//初始化时要保证normal状态,即 x&y pointer要实时更新,不应该是still
    	
        gyroOrientation[0] = 0.0f;
        gyroOrientation[1] = 0.0f;
        gyroOrientation[2] = 0.0f;
 
        // initialise gyroMatrix with identity matrix
        gyroMatrix[0] = 1.0f; gyroMatrix[1] = 0.0f; gyroMatrix[2] = 0.0f;
        gyroMatrix[3] = 0.0f; gyroMatrix[4] = 1.0f; gyroMatrix[5] = 0.0f;
        gyroMatrix[6] = 0.0f; gyroMatrix[7] = 0.0f; gyroMatrix[8] = 1.0f;
 
        // get sensorManager and initialize sensor listeners
        mSensorManager = sm;
        task  = new calculateFusedOrientationTask();
        initListeners();
        
        // wait for one second until gyroscope and magnetometer/accelerometer
        // data is initialised then scedule the complementary filter task
        //fuseTimer.scheduleAtFixedRate(new calculateFusedOrientationTask(),1000, TIME_CONSTANT);
        
        //mHandler = new Handler();
        d.setRoundingMode(RoundingMode.HALF_UP);
        d.setMaximumFractionDigits(3);
        d.setMinimumFractionDigits(3);
    }
    
    //handover the server between Poster/Mannequin & TV;
    //useless for now
    /*private void handoverOSCServer(String newIP){
    	sender.disconnect();
    	sender.connect(new InetSocketAddress(newIP, MSLConfig.server_port));
    }*/
    
    public void Stop() {
    	// unregister sensor listeners to prevent the activity from draining the device's battery.
    	mSensorManager.unregisterListener(this);
    	Log.v("test", "inside stoip");
    	//SensorFusionActivity0.recalibrate();
    	//fuseTimer.cancel();
    }
    
    // This function registers sensor listeners for the accelerometer, magnetometer and gyroscope.
    public void initListeners(){
        mSensorManager.registerListener(this,mSensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER),SensorManager.SENSOR_DELAY_GAME);
        mSensorManager.registerListener(this,mSensorManager.getDefaultSensor(Sensor.TYPE_GYROSCOPE),SensorManager.SENSOR_DELAY_GAME);
        
        mSensorManager.registerListener(this,mSensorManager.getDefaultSensor(Sensor.TYPE_MAGNETIC_FIELD),SensorManager.SENSOR_DELAY_GAME);
        mSensorManager.registerListener(this,mSensorManager.getDefaultSensor(Sensor.TYPE_ROTATION_VECTOR),SensorManager.SENSOR_DELAY_GAME);
        
        //mSensorManager.registerListener(this,mSensorManager.getDefaultSensor(Sensor.TYPE_ORIENTATION),SensorManager.SENSOR_DELAY_GAME);
        //mSensorManager.registerListener(this,mSensorManager.getDefaultSensor(Sensor.TYPE_LINEAR_ACCELERATION),SensorManager.SENSOR_DELAY_GAME);
        //mSensorManager.registerListener(this,mSensorManager.getDefaultSensor(Sensor.TYPE_GRAVITY),SensorManager.SENSOR_DELAY_GAME);
        //mSensorManager.registerListener(this,mSensorManager.getDefaultSensor(Sensor.TYPE_GAME_ROTATION_VECTOR),SensorManager.SENSOR_DELAY_GAME);
        //mSensorManager.registerListener(this,mSensorManager.getDefaultSensor(Sensor.TYPE_GEOMAGNETIC_ROTATION_VECTOR),SensorManager.SENSOR_DELAY_GAME);
    }

	@Override
	public void onAccuracyChanged(Sensor sensor, int accuracy) {		
	}

	@Override
	public void onSensorChanged(SensorEvent event) {
		switch(event.sensor.getType()) {
	    case Sensor.TYPE_ACCELEROMETER:
	        // copy new accelerometer data into accel array and calculate orientation
	        System.arraycopy(event.values, 0, accel, 0, 3);
	        calculateAccMagOrientation();
	        acceFunction(event);
	        Log.v("tag", "t oacce/0/"+event.values[0]);
	    	Log.v("tag", "t oacce/1/"+event.values[1]);
	    	Log.v("tag", "t oacce/2/"+event.values[2]);
	        break;
	 
	    case Sensor.TYPE_GYROSCOPE:
	    	Log.v("tag", "t gyro/0/"+event.values[0]);
	    	Log.v("tag", "t gyro/1/"+event.values[1]);
	    	Log.v("tag", "t gyro/2/"+event.values[2]);
	        gyroFunction_OSCPositionManager(event);
	        break;
	    
	    case Sensor.TYPE_ROTATION_VECTOR:
	    	//rotationFunction(event);
	    	Log.v("tag", "t rotv/0/"+event.values[0]);
	    	Log.v("tag", "t rotv/1/"+event.values[1]);
	    	Log.v("tag", "t rotv/2/"+event.values[2]);
	    	break;
	 
	    case Sensor.TYPE_MAGNETIC_FIELD:
	        // copy new magnetometer data into magnet array
	        System.arraycopy(event.values, 0, magnet, 0, 3);
	        magneticFunction(event);
	        break;
	        
	    //*** *** ***
	    //*** *** ***
	    /*case Sensor.TYPE_LINEAR_ACCELERATION:
	    	Log.v("tag", "t lacce/0/"+event.values[0]);
	    	Log.v("tag", "t lacce/1/"+event.values[1]);
	    	Log.v("tag", "t lacce/2/"+event.values[2]);
	    	break;
	    case Sensor.TYPE_GRAVITY:
	    	Log.v("tag", "t gra/0/"+event.values[0]);
	    	Log.v("tag", "t gra/1/"+event.values[1]);
	    	Log.v("tag", "t gra/2/"+event.values[2]);
	    	break;
	    case Sensor.TYPE_GAME_ROTATION_VECTOR:
	    	Log.v("tag", "t grota/0/"+event.values[0]);
	    	Log.v("tag", "t grota/1/"+event.values[1]);
	    	Log.v("tag", "t grota/2/"+event.values[2]);
	    	break;
	    case Sensor.TYPE_GEOMAGNETIC_ROTATION_VECTOR:
	    	Log.v("tag", "t georv/0/"+event.values[0]);
	    	Log.v("tag", "t georv/1/"+event.values[1]);
	    	Log.v("tag", "t georv/2/"+event.values[2]);
	    	break;*/
	    default:break;
	    }
		task.run();
	}
	
	private void acceFunction(SensorEvent event){
		float x = event.values[0];
		float y = event.values[1];
		float z = event.values[2];
		caliX = 0.2f*x + 0.8f*caliX;
		caliY = 0.2f*y + 0.8f*caliY;
		caliZ = 0.2f*z + 0.8f*caliZ;
		
		float xx= caliX - caliX_pre;
		float yy= caliY - caliY_pre;
		float zz= caliZ - caliZ_pre;
		Log.v("tag", "ggcali/ax/"+ d.format(xx) +"/" + d.format(y_pointer_cha));
		Log.v("tag", "ggcali/ay/"+ d.format(yy) +"/" + d.format(y_pointer_cha));
		Log.v("tag", "ggcali/az/"+ d.format(zz) +"/" + d.format(y_pointer_cha));
		
		caliX_pre = caliX;
		caliY_pre = caliY;
		caliZ_pre = caliZ;
	}
	
	private void magneticFunction(SensorEvent event){
		float x = event.values[0];
		float y = event.values[1];
		float z = event.values[2];
		magneX = 0.2f*x + 0.8f*magneX;
		magneY = 0.2f*y + 0.8f*magneY;
		magneZ = 0.2f*z + 0.8f*magneZ;
		
		float xx= magneX - magneX_pre;
		float yy= magneY - magneY_pre;
		float zz= magneZ - magneZ_pre;
		Log.v("tag", "ggcali/mx/"+ d.format(xx) +"/" + d.format(y_pointer_cha));
		Log.v("tag", "ggcali/my/"+ d.format(yy) +"/" + d.format(y_pointer_cha));
		Log.v("tag", "ggcali/mz/"+ d.format(zz) +"/" + d.format(y_pointer_cha));
		
		magneX_pre = magneX;
		magneY_pre = magneY;
		magneZ_pre = magneZ;
		
		/*msg = new OscMessage("/phone1"); // patcher style
		msg.addArgument(x);
		msg.addArgument(y);
		try {
			this.sender.sendPacket(msg);
			this.sender_tv.sendPacket(msg);
		} catch (InterruptedException e) {
			e.printStackTrace();
			Log.v("", "posisi/sensor network problem");
		}*/
	}
	
	/**
     * 2014-11-03 只是试验下不同的sensor的稳定性，是否可用于x、y的position
     * @param event
     * maybe try pointer_x
     */
    public void rotationFunction(SensorEvent event){
    	
    }
	
	// calculates orientation angles from accelerometer and magnetometer output
	public void calculateAccMagOrientation() {
	    if(SensorManager.getRotationMatrix(rotationMatrix, null, accel, magnet)) {
	        SensorManager.getOrientation(rotationMatrix, accMagOrientation);
	    }
	}
	
	// This function is borrowed from the Android reference
	// at http://developer.android.com/reference/android/hardware/SensorEvent.html#values
	// It calculates a rotation vector from the gyroscope angular speed values.
    private void getRotationVectorFromGyro(float[] gyroValues,
            float[] deltaRotationVector,
            float timeFactor)
	{
		float[] normValues = new float[3];
		
		// Calculate the angular speed of the sample
		float omegaMagnitude =
		(float)Math.sqrt(gyroValues[0] * gyroValues[0] +
		gyroValues[1] * gyroValues[1] +
		gyroValues[2] * gyroValues[2]);
		
		// Normalize the rotation vector if it's big enough to get the axis
		if(omegaMagnitude > EPSILON) {
		normValues[0] = gyroValues[0] / omegaMagnitude;
		normValues[1] = gyroValues[1] / omegaMagnitude;
		normValues[2] = gyroValues[2] / omegaMagnitude;
		}
		
		// Integrate around this axis with the angular speed by the timestep
		// in order to get a delta rotation from this sample over the timestep
		// We will convert this axis-angle representation of the delta rotation
		// into a quaternion before turning it into the rotation matrix.
		float thetaOverTwo = omegaMagnitude * timeFactor;
		float sinThetaOverTwo = (float)Math.sin(thetaOverTwo);
		float cosThetaOverTwo = (float)Math.cos(thetaOverTwo);
		deltaRotationVector[0] = sinThetaOverTwo * normValues[0];
		deltaRotationVector[1] = sinThetaOverTwo * normValues[1];
		deltaRotationVector[2] = sinThetaOverTwo * normValues[2];
		deltaRotationVector[3] = cosThetaOverTwo;
	}
    
    public boolean getIfKeepingXYPointerStill(){
    	return ifKeepingXYPointerStill;
    }
    
    void resetCounter(){
    	this.counter = 0;
    }
    
    /**
     * 自定义，用于是否保持 X Y 左边still静止
     * @param ifKeep
     */
    public static void setIfKeepingXYPointerStill (boolean ifKeep){
    	ifKeepingXYPointerStill = ifKeep;
    }
    //TODO
    static final float PARA_X = 25f;//5.1
    static final float PARA_Y = 75f;//39f
    public void gyroFunction_OSCPositionManager(SensorEvent event) {
    	//Log.v("coordinate", "still/if send/" + ifSendOSCPositionMessage);
		
        if (event.sensor.getType() == Sensor.TYPE_GYROSCOPE) {
			float dT = 0;
			if (timestamp != 0) {
				dT = (event.timestamp - timestamp) * NS2S;
				angle_x = (int) (event.values[X_Pointer_index] * 30f);// take two digits after decimal amount, multiply by 100, convert to Integer
				angle_y = (int) (event.values[Y_Pointer_index] * 80f);
				angle[X_Pointer_index] = angle[X_Pointer_index] + angle_x * dT;
				angle[Y_Pointer_index] = angle[Y_Pointer_index] + angle_y * dT;
				angle_pointer[X_Pointer_index] = 0.1f * SensorFusionActivity0_1IP_backup_beforeHelsinki.PARA_X * angle[X_Pointer_index] + 0.9f* angle_pointer[X_Pointer_index];// 5.1/8 is the coefficient to covert to coordinate
				angle_pointer[Y_Pointer_index] = 0.2f * SensorFusionActivity0_1IP_backup_beforeHelsinki.PARA_Y * angle[Y_Pointer_index] + 0.8f * angle_pointer[Y_Pointer_index];// 26 is the coefficient to covert to coordinate
				
			}
			timestamp = event.timestamp;
			x_pointer = (angle_pointer[X_Pointer_index] * (-1))/30f;// divide it by 100
			y_pointer = (angle_pointer[Y_Pointer_index]) / 80f;
			
			y_pointer_cha = y_pointer-y_pointer_pre;
			
			y_pointer_pre = y_pointer;
			
			msg = new OscMessage("/phone1"); // patcher style
			msg.addArgument(x_pointer + pointerShiftX);
			msg.addArgument(y_pointer);
				
			try {
				//this.sender.sendPacket(msg);
				this.sender_tv.sendPacket(msg);
				Log.v("", "posisi/x/"+x_pointer);
				Log.v("", "posisi/y/"+y_pointer);
			} catch (InterruptedException e) {
				e.printStackTrace();
				Log.v("", "posisi/sensor network problem");
			}
			
		}
    }
    
    /*public static void chooseScreen(int screen){
    	//Log.v("dir", "dir/1/~~"+MSLConfig.direction_value);
    	if(screen==1){
    		pointerShift = -7.333f;
    	}else if(screen==3){
    		pointerShift = 7.333f;
    	}else pointerShift = 0;
    }*/
	
    private float[] getRotationMatrixFromOrientation(float[] o) {
        float[] xM = new float[9];
        float[] yM = new float[9];
        float[] zM = new float[9];
     
        float sinX = (float)Math.sin(o[1]);
        float cosX = (float)Math.cos(o[1]);
        float sinY = (float)Math.sin(o[2]);
        float cosY = (float)Math.cos(o[2]);
        float sinZ = (float)Math.sin(o[0]);
        float cosZ = (float)Math.cos(o[0]);
     
        // rotation about x-axis (pitch)
        xM[0] = 1.0f; xM[1] = 0.0f; xM[2] = 0.0f;
        xM[3] = 0.0f; xM[4] = cosX; xM[5] = sinX;
        xM[6] = 0.0f; xM[7] = -sinX; xM[8] = cosX;
     
        // rotation about y-axis (roll)
        yM[0] = cosY; yM[1] = 0.0f; yM[2] = sinY;
        yM[3] = 0.0f; yM[4] = 1.0f; yM[5] = 0.0f;
        yM[6] = -sinY; yM[7] = 0.0f; yM[8] = cosY;
     
        // rotation about z-axis (azimuth)
        zM[0] = cosZ; zM[1] = sinZ; zM[2] = 0.0f;
        zM[3] = -sinZ; zM[4] = cosZ; zM[5] = 0.0f;
        zM[6] = 0.0f; zM[7] = 0.0f; zM[8] = 1.0f;
     
        // rotation order is y, x, z (roll, pitch, azimuth)
        float[] resultMatrix = matrixMultiplication(xM, yM);
        resultMatrix = matrixMultiplication(zM, resultMatrix);
        return resultMatrix;
    }
    
    private float[] matrixMultiplication(float[] A, float[] B) {
        float[] result = new float[9];
     
        result[0] = A[0] * B[0] + A[1] * B[3] + A[2] * B[6];
        result[1] = A[0] * B[1] + A[1] * B[4] + A[2] * B[7];
        result[2] = A[0] * B[2] + A[1] * B[5] + A[2] * B[8];
     
        result[3] = A[3] * B[0] + A[4] * B[3] + A[5] * B[6];
        result[4] = A[3] * B[1] + A[4] * B[4] + A[5] * B[7];
        result[5] = A[3] * B[2] + A[4] * B[5] + A[5] * B[8];
     
        result[6] = A[6] * B[0] + A[7] * B[3] + A[8] * B[6];
        result[7] = A[6] * B[1] + A[7] * B[4] + A[8] * B[7];
        result[8] = A[6] * B[2] + A[7] * B[5] + A[8] * B[8];
     
        return result;
    }
    
    class calculateFusedOrientationTask {
    //extends TimerTask {
        public void run() {
            float oneMinusCoeff = 1.0f - FILTER_COEFFICIENT;
            
            /*
             * Fix for 179� <--> -179� transition problem:
             * Check whether one of the two orientation angles (gyro or accMag) is negative while the other one is positive.
             * If so, add 360� (2 * math.PI) to the negative value, perform the sensor fusion, and remove the 360� from the result
             * if it is greater than 180�. This stabilizes the output in positive-to-negative-transition cases.
             */
            
            // azimuth
            if (gyroOrientation[0] < -0.5 * Math.PI && accMagOrientation[0] > 0.0) {
            	fusedOrientation[0] = (float) (FILTER_COEFFICIENT * (gyroOrientation[0] + 2.0 * Math.PI) + oneMinusCoeff * accMagOrientation[0]);
        		fusedOrientation[0] -= (fusedOrientation[0] > Math.PI) ? 2.0 * Math.PI : 0;
            }
            else if (accMagOrientation[0] < -0.5 * Math.PI && gyroOrientation[0] > 0.0) {
            	fusedOrientation[0] = (float) (FILTER_COEFFICIENT * gyroOrientation[0] + oneMinusCoeff * (accMagOrientation[0] + 2.0 * Math.PI));
            	fusedOrientation[0] -= (fusedOrientation[0] > Math.PI)? 2.0 * Math.PI : 0;
            }
            else {
            	fusedOrientation[0] = FILTER_COEFFICIENT * gyroOrientation[0] + oneMinusCoeff * accMagOrientation[0];
            }
            
            // pitch
            if (gyroOrientation[1] < -0.5 * Math.PI && accMagOrientation[1] > 0.0) {
            	fusedOrientation[1] = (float) (FILTER_COEFFICIENT * (gyroOrientation[1] + 2.0 * Math.PI) + oneMinusCoeff * accMagOrientation[1]);
        		fusedOrientation[1] -= (fusedOrientation[1] > Math.PI) ? 2.0 * Math.PI : 0;
            }
            else if (accMagOrientation[1] < -0.5 * Math.PI && gyroOrientation[1] > 0.0) {
            	fusedOrientation[1] = (float) (FILTER_COEFFICIENT * gyroOrientation[1] + oneMinusCoeff * (accMagOrientation[1] + 2.0 * Math.PI));
            	fusedOrientation[1] -= (fusedOrientation[1] > Math.PI)? 2.0 * Math.PI : 0;
            }
            else {
            	fusedOrientation[1] = FILTER_COEFFICIENT * gyroOrientation[1] + oneMinusCoeff * accMagOrientation[1];
            }
            
            // roll
            if (gyroOrientation[2] < -0.5 * Math.PI && accMagOrientation[2] > 0.0) {
            	fusedOrientation[2] = (float) (FILTER_COEFFICIENT * (gyroOrientation[2] + 2.0 * Math.PI) + oneMinusCoeff * accMagOrientation[2]);
        		fusedOrientation[2] -= (fusedOrientation[2] > Math.PI) ? 2.0 * Math.PI : 0;
            }
            else if (accMagOrientation[2] < -0.5 * Math.PI && gyroOrientation[2] > 0.0) {
            	fusedOrientation[2] = (float) (FILTER_COEFFICIENT * gyroOrientation[2] + oneMinusCoeff * (accMagOrientation[2] + 2.0 * Math.PI));
            	fusedOrientation[2] -= (fusedOrientation[2] > Math.PI)? 2.0 * Math.PI : 0;
            }
            else {
            	fusedOrientation[2] = FILTER_COEFFICIENT * gyroOrientation[2] + oneMinusCoeff * accMagOrientation[2];
            }
     
            // overwrite gyro matrix and orientation with fused orientation
            // to comensate gyro drift
            gyroMatrix = getRotationMatrixFromOrientation(fusedOrientation);
            System.arraycopy(fusedOrientation, 0, gyroOrientation, 0, 3);
            
            MSLConfig.direction_value = (int) (fusedOrientation[0] * 180/Math.PI) ;
            
            /**
             * 从(-180 ~ 0 ~ 180) 转化为 (0 ~ 360)
             */
            //MSLConfig.direction_value = (MSLConfig.direction_value + 360) % 360;
            
            /*MSLConfig.direction_value_rec = 0.3*MSLConfig.direction_value + 0.7*MSLConfig.direction_value_rec;
            if(MSLConfig.direction_value_rec>0 && MSLConfig.direction_value_rec<179){
            	MSLConfig.screenOnlyForMondayDemo = 1;
            	screenImageNr = 0;
            }else if(MSLConfig.direction_value_rec>179 || MSLConfig.direction_value_rec<0){
            	MSLConfig.screenOnlyForMondayDemo = 2;
            	screenImageNr = 2;
            }*/
            
            // update sensor output in GUI
            //mHandler.post(updateOreintationDisplayTask);
        }
    }
    
	public static void recalibrate() {
		angle[X_Pointer_index] = 5f;
		angle[Y_Pointer_index] = 0f;
		angle_pointer[X_Pointer_index] = 5f;
		angle_pointer[Y_Pointer_index] = 0f;
		Log.v("sensorcalibrate", "recalibrate");
	}
    
    
    // **************************** GUI FUNCTIONS *********************************

    
    public void updateOreintationDisplay() {
    	Log.v("test",d.format(fusedOrientation[0] * 180/Math.PI) + "****" );
    	
    	/*switch(radioSelection) {
    	case 0:
    		mAzimuthView.setText(d.format(accMagOrientation[0] * 180/Math.PI) + '�');
            mPitchView.setText(d.format(accMagOrientation[1] * 180/Math.PI) + '�');
            mRollView.setText(d.format(accMagOrientation[2] * 180/Math.PI) + '�');
    		break;
    	case 1:
    		mAzimuthView.setText(d.format(gyroOrientation[0] * 180/Math.PI) + '�');
            mPitchView.setText(d.format(gyroOrientation[1] * 180/Math.PI) + '�');
            mRollView.setText(d.format(gyroOrientation[2] * 180/Math.PI) + '�');
    		break;
    	case 2:
    		mAzimuthView.setText(d.format(fusedOrientation[0] * 180/Math.PI) + '�');
            mPitchView.setText(d.format(fusedOrientation[1] * 180/Math.PI) + '�');
            mRollView.setText(d.format(fusedOrientation[2] * 180/Math.PI) + '�');
    		break;
    	}*/
    }
    
    /**
     * 2014-10-31
     * 传感器 rotation_vector
     * @param se
     */
    public void ShelfVisualSearch_Status(SensorEvent se){
    	
    	
		double m = 2*Math.asin(se.values[0]);//[2]
		double z = Math.toDegrees(m);
		MSLConfig.direction_value = z;
		
		//---------------------
		/*if(2>0)
    		return;*/
		//---------------------
		/**
         * 转换
         * 从(-180 ~ 0 ~ 180) 转化为 (0 ~ 360)
         */
        MSLConfig.direction_value = ( - MSLConfig.direction_value + 360) % 360;
        
        /**
         * 滤波器
         */
        MSLConfig.direction_value_rec = 0.1*MSLConfig.direction_value + 0.9*MSLConfig.direction_value_rec;
        
        /**
         * 预防在0与360之间界限处，转换时造成的值变化太大
         */
        if(Math.abs(MSLConfig.direction_value-MSLConfig.direction_value_rec)>=200){
        	MSLConfig.direction_value_rec = MSLConfig.direction_value;
        }
        
        //2014-10-31 记录测direction的次数, 预防每次点击 后，从0重新开始计算造成一定的误差
        recorder++;
        if(recorder<=30) //过来45次后，在开始正常计算
        	return;
        
        /**
         * for SHELF-VisualSearch background drawable...
         * use in-out model
         * TODO here is shelf-border information
         */
        int BORDER1 = 20;
        int BORDER2 = 100;
        int BORDER1_OUT = BORDER1 - 15; //5
        int BORDER2_OUT = BORDER2 + 15; //115
        if(MSLConfig.direction_value_rec>=BORDER1 && MSLConfig.direction_value_rec<=BORDER2 ){
        	if(!INBORDER){
        		INBORDER = true;
        		//GantLondon0_pointer4pieces_1IP.updateBackgroundImages_handler.obtainMessage(GantLondon0_pointer4pieces_1IP.MSG_MSL_OSC_ShelfVisualSearch_Show, (int)MSLConfig.direction_value_rec).sendToTarget();
        	}
        }else if(MSLConfig.direction_value_rec<BORDER1_OUT || MSLConfig.direction_value_rec>BORDER2_OUT){
        	if(INBORDER){
        		INBORDER = false;
        		//GantLondon0_pointer4pieces_1IP.updateBackgroundImages_handler.obtainMessage(GantLondon0_pointer4pieces_1IP.MSG_MSL_OSC_ShelfVisualSearch_unShow, (int)MSLConfig.direction_value_rec).sendToTarget();
        	}
        }
        Log.v("dir", "dirdir/raw/"+MSLConfig.direction_value);
        Log.v("dir", "dirdir/new/"+MSLConfig.direction_value_rec);
        
        
        /**
         * TODO, for SCREEN / DIRECTION chosen
         * 比如 如果要用角度angle(而非image识别)来识别/区别 poster/mannequin/shelf，则需要此module
         */
        if(MSLConfig.direction_value_rec<=150){
        	MSLConfig.screenOnlyForMondayDemo = 1;//poster
        	screenImageNr = 0;
        }else{ //D-shop
        	MSLConfig.screenOnlyForMondayDemo = 2;
        	screenImageNr = 2;
        }
        }
    
    public static void chooseScreenOnlyForMondayDemo(int screen){
    	if(screen==1){
    		pointerShiftX = -5f;//poster
    		Log.v("dir", "dir/1/"+MSLConfig.direction_value);
    	}else if(screen==3){
    		pointerShiftX = 7.333f;
    	}else {
    		pointerShiftX = 5f;//D-shop
    		Log.v("dir", "dir/2/"+MSLConfig.direction_value);
    	}
    }
    
    private Runnable updateOreintationDisplayTask = new Runnable() {
		public void run() {
			updateOreintationDisplay();
		}
	};
}